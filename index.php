<?php
/*
 author: Jeorlie Edang
 email: edang.jeorlie@gmail.com

 BukoPHP v1
*/
error_reporting(-1);
ini_set("display_errors", true);
define('BUKO_PATH_ROOT', __dir__ .'/');
define('BUKO_PATH_SYSTEM', BUKO_PATH_ROOT .'system/');
define('BUKO_PATH_APP', BUKO_PATH_ROOT .'app/');
define('BUKO_PATH_HELPERS', BUKO_PATH_APP .'helpers/');
define('BUKO_PATH_TEMPLATES', BUKO_PATH_APP .'templates/');
define('BUKO_PATH_INCLUDES', BUKO_PATH_APP .'includes/');
define('BUKO_PATH_PLUGINS', BUKO_PATH_APP .'plugins/');
require BUKO_PATH_SYSTEM .'index.php';
