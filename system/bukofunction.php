<?php
/*
 author: Jeorlie Edang
 email: edang.jeorlie@gmail.com

*/

function buko_debug($v){
  echo '<pre>';print_r($v);echo '</pre>';
}
//--------------
function buko_sanitize($str, $long = FILTER_SANITIZE_STRING){
  return filter_var(trim($str), $long);
}
//--------------
function buko_json_error($r){
  $obj = array('error' => true);
  if(!is_array($r)) {
    $obj['message'] = $r;    
  } else {
     $obj['message'] = $r['message']; unset($r['message']);
     $obj['data'] = $r;
  }
  buko_json_header($obj);
}
//--------------
function buko_json_success($r){
  $obj = array('success' => true);
  if(!is_array($r)) {
    $obj['message'] = $r;    
  } else {
     $obj['message'] = $r['message']; unset($r['message']);
     $obj['data'] = $r;
  }
  buko_json_header($obj);
}
//--------------
function buko_json_header($arr){
  header("Content-type: application/json");
  echo json_encode($arr);
  exit;
}
//--------------