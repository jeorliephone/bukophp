<?php
/*
 author: Jeorlie Edang
 email: edang.jeorlie@gmail.com

*/


$GLOBALS['buko_config']['app']['title'] = 'Buko PHP';
$GLOBALS['buko_config']['app']['version'] = '1.0';


$GLOBALS['buko_config']['app']['base_url'] = 'http://localhost/test/bukophp/';
/*
// [ MySQL Config ]
$buko_config['db']['host'] = 'localhost';
//you may change this according to your database name
$buko_config['db']['dbname'] = 'buko'; 
$buko_config['db']['user'] = 'root';
$buko_config['db']['password'] = 'root';
*/

/*
[SQLite Config]
*/
$GLOBALS['buko_config']['db']['path'] = '';