<?php
/*
 author: Jeorlie Edang
 email: edang.jeorlie@gmail.com
*/

require __dir__ .'/bukofunction.php';
require __dir__ .'/bukoconfig.php';
require __dir__ .'/bukowhitelisturl.php';


$url = explode("/", ltrim($_SERVER['QUERY_STRING'], "/"));


if(strpos($url[0], "index") !== false) header('Location: '. $GLOBALS['buko_config']['app']['base_url']);

if(empty($url[0]) && strlen($url[0]) == 0) {
  $url[0] = 'index';
}

if(!isset($buko_whitelist[$url[0]])){
  buko_json_error("URL not allowed.");
}
$file = BUKO_PATH_APP .'class/'.$buko_whitelist[$url[0]].'/index.php';

if(!file_exists($file)) buko_json_error("Class file not found.");
else require $file;
$md = 'app_module_'. $buko_whitelist[$url[0]];
if(!class_exists($md)) buko_json_error("Class instance not found.");

$app = new $md();
$app->Init();